# Team Name: Raspberry Spy #

Team Members: Yuriy Dzyuba and Kevin Chetty

### Overview ###

 When thinking about what to do for our project, we imposed on ourselves a major guideline that our project must conform to. The guideline was that whatever we would decide on, it had to be relevant to the real world today and reflective of the emerging technology. Operating by this guideline has led us to choose to create a surveillance vehicle that will be controlled over the internet. In addition to being controlled over the internet, the car will have on board surveillance equipment that will be controlled with a wireless rf remote.  With autonomous vehicles on the rise and the need for vehicle to vehicle communication, we think this is a great project that is applicable to today�s world.



### Further Information ###

To learn more, read the Wiki page for the individual modules and the final report. You can also watch a video presentation [here](https://youtu.be/VmvoMlB_Ids)