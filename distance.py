import RPi.GPIO as GPIO
import time
import datetime
ts = time.time()

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

TRIG = 23
ECHO = 24


GPIO.setup(TRIG, GPIO.OUT)
GPIO.setup(ECHO, GPIO.IN)

GPIO.output(TRIG, False)
print "hello"
time.sleep(2)

GPIO.output(TRIG, True)
time.sleep(0.00001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO) == 0:
  pulse_start = time.time()
  
while GPIO.input(ECHO) == 1:
  pulse_end = time.time()
  
pulse_duration = pulse_end - pulse_start

distance = pulse_duration * 17150

distance = round(distance, 2)

print "Distance: ",distance, "cm"

st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d  %H:%M:%S')
print st
file = open("myTestFile.txt" , "w")
file.write('The object is {0} cm away from the surveillance vehicle. \nThis picture was taken with the RF remote at {1}'.format(distance, st))
file.close

GPIO.cleanup() 